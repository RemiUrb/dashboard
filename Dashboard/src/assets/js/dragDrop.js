var gridTarget = interact.snappers.grid({
    x: 50,
    y: 50,
    range: 10,
    offset: { x: 5, y: 10 },
    limits: {
      top: 0,
      left: 0,
      bottom: 500,
      height: 500
    }
  })
  
  ////////////////////////////////////////////
  
  
  interact('.draggable')
    .draggable({
      ignoreFrom: '.unDraggable',
      modifiers: [
        interact.modifiers.snap({
          targets: [
            interact.snappers.grid({ x: 30, y: 30 })
          ],
          range: Infinity,
          relativePoints: [ { x: 0, y: 0 } ]
        }),
        interact.modifiers.restrictRect({
          restriction: 'parent',
          endOnly: true
        }),
          interact.modifiers.snap({ targets: [gridTarget] })
      ],
      autoScroll: true,
      listeners: {
        move: dragMoveListener,
        end (event) {
            var target = event.target;
            console.log(target);
            console.log(target.getAttribute('data-x'),target.getAttribute('data-y'));
        }
      }
    })
  
  function dragMoveListener (event) {
    var target = event.target
    var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx
    var y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy
    target.style.webkitTransform =
      target.style.transform =
        'translate(' + x + 'px, ' + y + 'px)'
    target.setAttribute('data-x', x)
    target.setAttribute('data-y', y)
  }