import { Component, OnInit, Input } from '@angular/core';
import { Memo } from '../memo';
import { MemosComponent } from '../memos/memos.component';
@Component({
  selector: 'app-memo-detail',
  templateUrl: './memo-detail.component.html',
  styleUrls: ['./memo-detail.component.css']
})
export class MemoDetailComponent implements OnInit {
  @Input() memo: Memo;
  constructor() { }

  ngOnInit(): void {
  }
  deselectmemo2(): void {
    document.getElementById("cache1").style.visibility = "hidden";
    document.getElementById("cache2").style.visibility = "hidden";
  }

}
