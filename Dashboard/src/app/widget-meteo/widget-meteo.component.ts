import {
  Component
} from "@angular/core";
import {
  HttpClient
} from "@angular/common/http";


@Component({
  selector: "app-widget-meteo",
  templateUrl: "./widget-meteo.component.html",
  styleUrls: ["./widget-meteo.component.css"]
})

export class WidgetMeteoComponent {

  icon;
  wind;
  humidity;
  temperature;
  meteo;
  localisation;
  ville = "Lille";
  selectElem
  urlmeteo = "http://api.openweathermap.org/data/2.5/weather?q=" + this.ville + "&appid=a054d89b0a37924a5bbc2ab63a4e1d5b";

  constructor(private HttpClient: HttpClient) {}

  ngOnInit() {
    this.sendGetMeteo();
  }

  //Fonction pour rafraichir la meteo
  sendGetMeteo() {
    console.log(this.urlmeteo);
    this.HttpClient.get(this.urlmeteo).subscribe((res: any) => {
      this.temperature = Math.round(res.main.temp - 273);
      this.meteo = res.weather[0].main;
      this.wind = res.wind.speed;
      this.humidity = res.main.humidity;
      this.localisation = res.name;
      this.icon = parseInt(res.weather[0].icon.substr(0, 2));
      console.log(this.icon)

      switch (this.icon) {
        case 1:
          this.icon = "assets/icons/weather/sun.png"
          break;
        case 2:
          this.icon = "assets/icons/weather/cloud.png"
          break;
        case 3:
          this.icon = "assets/icons/weather/cloud.png"
          break;
        case 4:
          this.icon = "assets/icons/weather/cloud.png"
          break;
        case 50:
          this.icon = "assets/icons/weather/cloud.png"
          break;
        case 9:
          this.icon = "assets/icons/weather/rain.png"
          break;
        case 10:
          this.icon = "assets/icons/weather/cloud.png"
          break;
        case 11:
          this.icon = "assets/icons/weather/cloud.png"
          break;
        case 13:
          this.icon = "assets/icons/weather/cloud.png"
          break;
      }
      console.log(this.icon);
    });
  }

}
