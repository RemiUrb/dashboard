import { Component, OnInit } from '@angular/core';
import {NgWhiteboardService} from 'ng-whiteboard';

@Component({
  selector: 'app-whiteboard',
  templateUrl: './whiteboard.component.html',
  styleUrls: ['./whiteboard.component.css']
})
export class WhiteboardComponent implements OnInit {

  constructor(private whiteboardService: NgWhiteboardService) { 
    this.whiteboardService.save();
  }

  ngOnInit(): void {
  }

  eraseWhiteboard(){
    console.log("efface whiteboard");
    this.whiteboardService.erase();
  }

}
