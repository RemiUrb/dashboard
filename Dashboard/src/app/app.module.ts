import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CalendrierComponent} from './calendrier/calendrier.component';
import { ScheduleModule, RecurrenceEditorModule, DayService, WeekService, WorkWeekService, MonthService, MonthAgendaService} from '@syncfusion/ej2-angular-schedule';
import { HttpClientModule } from '@angular/common/http';
import { WidgetMeteoComponent } from './widget-meteo/widget-meteo.component';
import { WidgetHorlogeComponent } from './widget-horloge/widget-horloge.component';
import { CalculatriceComponent } from './calculatrice/calculatrice.component';
import {MatTableModule} from '@angular/material/table';
import { UtilisateursComponent } from './utilisateurs/utilisateurs.component';
import { FormsModule } from '@angular/forms';
import { NgWhiteboardModule } from 'ng-whiteboard';
import { WhiteboardComponent } from './whiteboard/whiteboard.component';



@NgModule({
  declarations: [
    AppComponent,
	CalendrierComponent,
    WidgetMeteoComponent,
    WidgetHorlogeComponent,
    CalculatriceComponent,
    UtilisateursComponent,
    WhiteboardComponent
    
  ],
  imports: [
    BrowserModule,
    ScheduleModule,
    RecurrenceEditorModule,
    HttpClientModule,
    MatTableModule,
    FormsModule,
	NgWhiteboardModule,
    HttpClientModule
  ],
  providers:[DayService, WeekService, WorkWeekService,MonthService, MonthAgendaService],
  bootstrap: [AppComponent]
})
export class AppModule { }