import { Component } from '@angular/core';
import { CookieService } from 'ngx-cookie-service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Dashboard';
  constructor(private cookieService:CookieService) {}
  elem=document.documentElement;
  fullscreen(){
    if(this.elem.requestFullscreen){
      this.elem.requestFullscreen();
    }
  }
  getCookie(cookieName:string){
    return this.cookieService.get(cookieName)
  }

  getDay(){
    const date = new Date();
    const options = { day: 'numeric'};
    return date.toLocaleDateString('fr-FR', options);
  }

  getMonth(){
    const date = new Date();
    const options = { month: 'long'};
    return date.toLocaleDateString('fr-FR', options);
  }
}