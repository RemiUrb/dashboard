import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CalendrierComponent } from './calendrier.component';

describe('CalendrierComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        CalendrierComponent
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(CalendrierComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'scheduler'`, () => {
    const fixture = TestBed.createComponent(CalendrierComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('scheduler');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(CalendrierComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('scheduler app is running!');
  });
});
