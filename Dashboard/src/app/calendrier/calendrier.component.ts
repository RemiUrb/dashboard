import { Component } from '@angular/core';
import { View, EventSettingsModel } from '@syncfusion/ej2-angular-schedule';
import{DataManager,WebApiAdaptor} from'@syncfusion/ej2-data';

@Component({
  selector: 'app-calendrier',
  template: '<ejs-schedule height="100%" width="100%" [eventSettings]="eventObject" ></ejs-schedule>',//pour changer la taille du calendrier
  //templateUrl: './app.component.html',
  styleUrls: ['./calendrier.component.css']
})
export class CalendrierComponent {
  title = 'scheduler';

  private eventData: DataManager = new DataManager({
    url:"https://ej2services.syncfusion.com/production/web-services/api/Schedule",
    adaptor: new WebApiAdaptor,
    crossDomain:true,
  })
  public eventObject: EventSettingsModel = {
    dataSource:this.eventData,
  }
}
