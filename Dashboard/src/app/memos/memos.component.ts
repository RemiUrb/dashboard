import { Component, OnInit } from '@angular/core';
import { Memo } from '../memo';
import { MemoService } from '../memo.service';
@Component({
  selector: 'app-memos',
  templateUrl: './memos.component.html',
  styleUrls: ['./memos.component.css']
})
export class MemosComponent implements OnInit {
  
  selectedMemo: Memo;
  deselectedMemo: Memo;
  memos: Memo[];
  constructor(private memoService: MemoService) {}

  ngOnInit() {
	  this.getMemos();
  }
  onSelect(memo: Memo): void {
    this.selectedMemo = memo;
    document.getElementById("cache1").style.visibility = "visible";
    document.getElementById("cache2").style.visibility = "visible";
  }
  deselectmemo(): void {
    this.selectedMemo = this.deselectedMemo;
    console.log("marche");
  }
  getMemos(): void {
    this.memoService.getMemos()
	.subscribe(memos => this.memos = memos);
  }
}