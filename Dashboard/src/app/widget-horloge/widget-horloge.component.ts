import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-widget-horloge',
  templateUrl: './widget-horloge.component.html',
  styleUrls: ['./widget-horloge.component.css']
})
export class WidgetHorlogeComponent implements OnInit {

  todayDate=new Date();
  x;
  
  constructor() { }

  ngOnInit(): void {
    //Rafraichisement de l'heure toutes les 5 secondes
    this.x = setInterval(() => { this.rafraichir(); }, 5000);
  }

  rafraichir(){
    console.log("Rafraichisement Heure");
    this.todayDate=new Date();
  }

}
