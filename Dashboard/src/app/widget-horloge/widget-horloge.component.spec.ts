import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetHorlogeComponent } from './widget-horloge.component';

describe('WidgetHorlogeComponent', () => {
  let component: WidgetHorlogeComponent;
  let fixture: ComponentFixture<WidgetHorlogeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WidgetHorlogeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetHorlogeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
