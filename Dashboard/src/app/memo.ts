export interface Memo {
  id: number;
  name: string;
  details: string;
}