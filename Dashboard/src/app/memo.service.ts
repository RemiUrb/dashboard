import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { Memo } from './memo';
import { MEMOS } from './defaultmemos';
@Injectable({
  providedIn: 'root',
})
export class MemoService {

  constructor() { }
  
  getMemos(): Observable<Memo[]> {
  return of(MEMOS);
}
}
