import { Component } from '@angular/core';
import {utilisateurs} from './utilisateurs';
import {HttpClient} from '@angular/common/http'
import { environment } from 'src/environments/environment';

 
@Component({
  selector: 'app-utilisateurs',
  templateUrl: './utilisateurs.component.html',
  styleUrls: ['./utilisateurs.component.css']
})
export class UtilisateursComponent {
  utilisateursList : any

  constructor (private Httpclient:HttpClient){
    this.Httpclient.get(this.apiUrl + '/users').subscribe((response) =>{
      this.utilisateursList = response
      console.log(response)
    });
  }

  apiUrl:string = environment.apiUrl + '/admin'

  editField: string="";
  isEditing: boolean = false;
  enableEditIndex =null;
  UsersArray: Array<utilisateurs> = [];  
  newDynamic: any = {};  

   add() {
    this.newDynamic = {name: "",firstName:"",username: "",Password:""};  
    this.utilisateursList.push(this.newDynamic);
    console.log(this.utilisateursList);  
    return true; 
    }
  

remove(id: any) { //supprimer users , delete in bdd too
    let username = this.utilisateursList[id].username
    this.utilisateursList.splice(id, 1)
    this.Httpclient.post(this.apiUrl + '/remove', {username:username}).subscribe(
      () => {}, 
      (error) => console.log(error))
}

  switchEditMode(i: any ) { //edit at save button
    this.isEditing = true;
    this.enableEditIndex = i;
  }
  save(id: any) { //save info , send to bdd
    this.isEditing = false
    this.enableEditIndex = null;
    console.log(this.utilisateursList);
    let username = this.utilisateursList[id].username
    this.Httpclient.post(this.apiUrl + '/edit', {user:this.utilisateursList[id]}).subscribe(
      () => {}, 
      (error) => console.log(error))
  }

  
}